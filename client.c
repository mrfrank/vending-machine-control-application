/*
 * SPDX-FileCopyrightText: Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Oniro Project Vending machine control application - client (server stub)
 *
 */

#include <libwebsockets.h>
#include <json-c/json.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

static struct lws *web_socket = NULL;

#define RX_BUFFER_BYTES (1024)

json_object * set_property()
{
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();

	/*Creating a json string*/
	json_object *jstring = json_object_new_string("setProperty");

	/*Creating a json array*/
	json_object *jarray = json_object_new_array();

	/*Creating json strings*/
	json_object *jstring1 = json_object_new_int(0);
	json_object *jstring2 = json_object_new_int(1);
	json_object *jstring3 = json_object_new_int(0);
	json_object *jstring4 = json_object_new_int(1);

	/*Adding the above created json strings to the array*/
	json_object_array_add(jarray,jstring1);
	json_object_array_add(jarray,jstring2);
	json_object_array_add(jarray,jstring3);
	json_object_array_add(jarray,jstring4);

	json_object * jobj1 = json_object_new_object();

	json_object_object_add(jobj,"messageType", jstring);
	json_object_object_add(jobj1,"selection", jarray);
	json_object_object_add(jobj,"data", jobj1);

	/*Now printing the json object*/
	printf ("\nThe json object created:\n %s",json_object_to_json_string(jobj));

	int length = strlen(json_object_to_json_string(jobj));

	printf ("\nPayload length:%d\n", length );

	return jobj;
}

json_object *request_action()
{
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();

	/*Creating a json string*/
	json_object *jstring = json_object_new_string("requestAction");

	/*Creating a json array*/
	json_object *jarray = json_object_new_array();

	/*Creating json strings*/
	json_object *jstring1 = json_object_new_int(0);
	json_object *jstring2 = json_object_new_int(1);
	json_object *jstring3 = json_object_new_int(0);
	json_object *jstring4 = json_object_new_int(1);

	/*Adding the above created json strings to the array*/
	json_object_array_add(jarray,jstring1);
	json_object_array_add(jarray,jstring2);
	json_object_array_add(jarray,jstring3);
	json_object_array_add(jarray,jstring4);

	json_object * jobj1 = json_object_new_object();
	json_object * jobj2 = json_object_new_object();
	json_object * jobj3 = json_object_new_object();

	json_object_object_add(jobj,"messageType", jstring);
	json_object_object_add(jobj3,"selection", jarray);
	json_object_object_add(jobj2,"input", jobj3);
	json_object_object_add(jobj1,"deliver", jobj2);
	json_object_object_add(jobj,"data", jobj1);

	/*Now printing the json object*/
	printf ("\nThe json object created:\n %s",json_object_to_json_string(jobj));

	int length = strlen(json_object_to_json_string(jobj));

	printf ("\nPayload length:%d\n", length );

	return jobj;
}

json_object * add_event_subscription()
{
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();

	/*Creating a json string*/
	json_object *jstring = json_object_new_string("addEventSubscription");

	/*Creating a json array*/
	json_object *jarray = json_object_new_array();

	json_object * jobj1 = json_object_new_object();

	json_object_object_add(jobj,"messageType", jstring);
	json_object_object_add(jobj1,"delivered", jarray);
	json_object_object_add(jobj,"data", jobj1);

	/*Now printing the json object*/
	printf ("\nThe json object created:\n %s",json_object_to_json_string(jobj));

	int length = strlen(json_object_to_json_string(jobj));

	printf ("\nPayload length:%d\n", length );

	return jobj;
}

static int client_callback( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )
{
	switch( reason )
	{
		case LWS_CALLBACK_CLIENT_ESTABLISHED:
			printf("Client & Server connection established\n");
			lws_callback_on_writable( wsi );
			break;

		case LWS_CALLBACK_CLIENT_RECEIVE:
			printf("\nReceieved data from Server! \n %s \n", (char *)in);
			break;

		case LWS_CALLBACK_CLIENT_WRITEABLE:
		{
			int length;
			json_object *jobj = json_object_new_object();

			jobj = set_property();
			length = strlen(json_object_to_json_string(jobj));
			lws_write(wsi, (unsigned char *)json_object_to_json_string(jobj), length , LWS_WRITE_TEXT );
			sleep(3);

			jobj = request_action();
			length = strlen(json_object_to_json_string(jobj));
			lws_write(wsi, (unsigned char *)json_object_to_json_string(jobj), length , LWS_WRITE_TEXT );
			sleep(3);

			jobj = add_event_subscription();
			length = strlen(json_object_to_json_string(jobj));
			lws_write(wsi, (unsigned char *)json_object_to_json_string(jobj), length , LWS_WRITE_TEXT );
			sleep(3);

			break;
		}
		case LWS_CALLBACK_CLOSED:
		case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
			web_socket = NULL;
			break;

		default:
			break;
	}
	return 0;
}

enum protocols
{
	PROTOCOL_EXAMPLE = 0,
	PROTOCOL_COUNT
};

static struct lws_protocols protocols[] =
{
	{
		"lws-protocol",
		client_callback,
		0,
		RX_BUFFER_BYTES,
	},
	{ NULL, NULL, 0, 0 } /* terminator */
};

int main( int argc, char *argv[] )
{
	struct lws_context_creation_info info;
	memset( &info, 0, sizeof(info) );

	info.port = CONTEXT_PORT_NO_LISTEN;
	info.protocols = protocols;
	info.gid = -1;
	info.uid = -1;

	struct lws_context *context = lws_create_context( &info );

	time_t old = 0;
	while( 1 )
	{
		const int timeout_ms = 250;
		struct timeval tv;
		gettimeofday( &tv, NULL );

		/* Connect if we are not connected to the server. */
		if( !web_socket && tv.tv_sec != old )
		{
			struct lws_client_connect_info ccinfo = {0};
			ccinfo.context = context;
			ccinfo.address = "localhost";
			if ( argc == 1 ) {
				/* assign the default port, if port number is not specified */
				ccinfo.port = 8888;
			} else {
				ccinfo.port = atoi( argv[1] );
			}
			ccinfo.path = "/";
			ccinfo.host = lws_canonical_hostname( context );
			ccinfo.origin = "origin";
			ccinfo.protocol = protocols[PROTOCOL_EXAMPLE].name;
			web_socket = lws_client_connect_via_info(&ccinfo);
		}

		lws_service( context, timeout_ms );
	}
	lws_context_destroy( context );
	return 0;
}
