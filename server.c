/*
 * SPDX-FileCopyrightText: Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Oniro Project Vending machine control application - server
 *
 */

#include <libwebsockets.h>
#include <json-c/json.h>
#include <string.h>
#include <stdio.h>
#include <systemd/sd-daemon.h>
#include <pim435/pim435.h>

#ifndef RX_BUFFER_BYTES
#define RX_BUFFER_BYTES (1024)
#endif

#ifndef VENDING_MACHINE_ACTION_DELAY
#define VENDING_MACHINE_ACTION_DELAY (10)
#endif

#ifdef DEBUG
#define debugf printf
#else
#define debugf(...)
#endif

/* Function Prototye */

void set_selection(pim435_context* pim435_ctx, int selection[], int length,
		   unsigned char red, unsigned char green, unsigned char blue);

void msg_parser(const char * message_type, json_object *selection);
void set_property(void *in);
void request_action(void *in);
void add_event_subscription(void *in);
json_object * event();
void send_process_completed(struct lws *wsi);
static int server_callback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

pim435_context* pim435_ctx; /* File descriptor for i2c dev TODO move to context structure */

void msg_parser(const char * message_type, json_object *selection)
{
	json_object *data_buff;
	int s_len = json_object_array_length(selection);
	int data[s_len], x, y;
	unsigned char r=0,g=0,b=0;

	if(strcmp("setProperty", message_type) == 0) {
		r = 0xFF;
		g = 0x00;
		b = 0x00;
	} else if(strcmp("requestAction", message_type) == 0) {
		r = 0x00;
		g = 0x00;
		b = 0xFF;
	}
	for(int i=0;i<s_len;i++) {
		data_buff = json_object_array_get_idx(selection, i);
		int item = json_object_get_int(data_buff);
		data[i]=item;
	}
	set_selection(pim435_ctx, data, s_len, r, g, b);
}

void set_selection(pim435_context* pim435_ctx, int selection[], int length,
		   unsigned char red, unsigned char green, unsigned char blue)
{
	if (length > PIM435_VIEWPORT_WIDTH*PIM435_VIEWPORT_HEIGHT) {
   		printf("warning: Selection is too large for controler %d\n", length);
   		length = PIM435_VIEWPORT_WIDTH*PIM435_VIEWPORT_HEIGHT;
	}

	for(int i=0; i<length; i++)  {
		int x = i / PIM435_VIEWPORT_WIDTH;
		int y = i % PIM435_VIEWPORT_HEIGHT;
   		if (selection[i]) {
			pim435_set_pixel(pim435_ctx, x, y, red, green, blue);
		} else {
			pim435_set_pixel(pim435_ctx, x, y, 0x00, 0x00, 0x00);
		}
	}
	pim435_show(pim435_ctx);
}

void set_property(void *in)
{
	json_object *parsed_json;
	json_object *messageType;
	json_object *selection;
	json_object *data;
	json_object *data_buff;

	parsed_json = json_tokener_parse(in);

	json_object_object_get_ex(parsed_json, "messageType", &messageType);
	json_object_object_get_ex(parsed_json, "selection", &selection);
	json_object_object_get_ex(parsed_json, "data", &data);

	debugf("\nMessage Type: %s\n", json_object_get_string(messageType));

	const char *message_type = json_object_get_string(messageType);

	parsed_json = json_tokener_parse(json_object_to_json_string(data));
	json_object_object_get_ex(parsed_json, "selection", &selection);

	debugf("data: %s\n", json_object_to_json_string(data));

	msg_parser(message_type, selection);
}

void request_action(void *in)
{
	json_object *parsed_json;
	json_object *messageType;
	json_object *selection;
	json_object *data;
	json_object *data_buff;
	json_object *deliver;
	json_object *input;

	parsed_json = json_tokener_parse(in);

	json_object_object_get_ex(parsed_json, "messageType", &messageType);
	json_object_object_get_ex(parsed_json, "selection", &selection);
	json_object_object_get_ex(parsed_json, "data", &data);
	json_object_object_get_ex(parsed_json, "deliver", &deliver);
	json_object_object_get_ex(parsed_json, "input", &input);

	debugf("Message Type: %s\n", json_object_get_string(messageType));

	const char *message_type = json_object_get_string(messageType);

	parsed_json = json_tokener_parse(json_object_to_json_string(data));
	json_object_object_get_ex(parsed_json, "deliver", &deliver);
	parsed_json = json_tokener_parse(json_object_to_json_string(deliver));
	json_object_object_get_ex(parsed_json, "input", &input);
	parsed_json = json_tokener_parse(json_object_to_json_string(input));
	json_object_object_get_ex(parsed_json, "selection", &selection);

	debugf("data: %s\n", json_object_to_json_string(data));

	int s_len = json_object_array_length(selection);

	for(int i=0;i<s_len;i++) {
		data_buff = json_object_array_get_idx(selection, i);
		debugf("%d\n",json_object_get_int(data_buff));
	}
	debugf("\n");

        msg_parser(message_type, selection);
}

void add_event_subscription(void *in)
{
	json_object *parsed_json;
	json_object *messageType;
	json_object *selection;
	json_object *data;

	parsed_json = json_tokener_parse(in);

	json_object_object_get_ex(parsed_json, "messageType", &messageType);
	json_object_object_get_ex(parsed_json, "delivered", &selection);
	json_object_object_get_ex(parsed_json, "data", &data);

	debugf("Message Type: %s\n", json_object_get_string(messageType));

	parsed_json = json_tokener_parse(json_object_to_json_string(data));
	json_object_object_get_ex(parsed_json, "selection", &selection);

	debugf("data: %s\n", json_object_to_json_string(data));
}

json_object * event()
{
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();

	/*Creating a json string*/
	json_object *jstring = json_object_new_string("event");

	/*Creating a json array*/
	json_object *jarray = json_object_new_array();

	/*Creating a json object*/
	json_object * jobj1 = json_object_new_object();
	json_object * jobj2 = json_object_new_object();

	json_object_object_add(jobj,"messageType", jstring);
	json_object_object_add(jobj1,"delivered", jobj2);
	json_object_object_add(jobj,"data", jobj1);

	debugf("\nMessage Type: %s\n", json_object_get_string(jstring));
	debugf("data: %s\n", json_object_to_json_string(jobj1));
	debugf("\n");

	return jobj;
}

void send_process_completed(struct lws *wsi)
{
    int length;
    json_object *jobj = json_object_new_object();

    jobj = event();
    length = strlen(json_object_to_json_string(jobj));
    lws_write(wsi, (unsigned char *)json_object_to_json_string(jobj), length , LWS_WRITE_TEXT );

    debugf("Event sent to client - Process Completed!\n");
}

static int server_callback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len)
{
	switch( reason )
	{
		case LWS_CALLBACK_ESTABLISHED:
			debugf("Connection established\n");
			break;

		case LWS_CALLBACK_RECEIVE:
		{
			json_object *parsed_json;
			json_object *messageType;
			parsed_json = json_tokener_parse(in);
			json_object_object_get_ex(parsed_json, "messageType", &messageType);

			if(strcmp("setProperty", json_object_get_string(messageType)) == 0)
			{
				set_property(in);
			}
			if (strcmp("requestAction", json_object_get_string(messageType)) == 0)
			{
				request_action(in);
				sleep(VENDING_MACHINE_ACTION_DELAY);
				send_process_completed(wsi);
			}
			if (strcmp("addEventSubscription", json_object_get_string(messageType)) == 0)
			{
				add_event_subscription(in);
				lws_callback_on_writable_all_protocol( lws_get_context( wsi ), lws_get_protocol( wsi ) );
			}
			break;
		}
		default:
			break;
	}
	return 0;
}

enum protocols
{
	PROTOCOL_HTTP = 0,
	PROTOCOL_EXAMPLE,
	PROTOCOL_COUNT
};

static struct lws_protocols protocols[] =
{
	{
		"lws-protocol",
		server_callback,
		0,
		RX_BUFFER_BYTES,
	},
	{ NULL, NULL, 0, 0 } /* terminator */
};

int main(int argc, char *argv[])
{
	char *get_i2c_bus;
	int i2c_bus = PIM435_I2C_BUS; /* default i2c bus number is 1. */
	const int timeout_ms = 1000000;
	struct lws_context_creation_info info;

	memset( &info, 0, sizeof(info) );

	if ((get_i2c_bus = getenv("VENDING_MACHINE_CONTROL_I2C_BUS")) != NULL) {
		i2c_bus = atoi(get_i2c_bus);
	}

	pim435_ctx = pim435_open(i2c_bus, PIM435_I2C_ADDRESS);
	if (pim435_ctx <= 0) {
                debugf("error: io:\n");
		exit(1);
        }

	if ( argc == 1 ) {
		/* assign the default port, if port number is not specified */
		info.port = 8888;
	} else {
		info.port = atoi( argv[1] );
	}

	info.protocols = protocols;
	info.gid = -1;
	info.uid = -1;

	struct lws_context *context = lws_create_context( &info );

	sd_notify(0, "READY=1");
	while( 1 )
	{
		lws_service( context, timeout_ms );
	}

	lws_context_destroy( context );
	lwsl_notice("libwebsockets server code exited cleanly\n");

	return 0;
}
