#!/usr/bin/env python3
# -*- mode: python; python-indent-offset: 4; indent-tabs-mode: nil -*-
# SPDX-FileCopyrightText: Huawei Inc.
# SPDX-License-Indentifier: Apache-2.0
# Contact: Philippe Coval <philippe.coval@huawei.com>

# Usage:
# pip3 install --user webthing

import time
import uuid

from webthing import (Property, SingleThing, Thing, Value, Action, Event,
                      WebThingServer)

class DeliveredEvent(Event):
    def __init__(self, thing, data):
        Event.__init__(self, thing, 'delivered', data=data)

class DeliverAction(Action):
    def __init__(self, thing, input_):
        Action.__init__(self, uuid.uuid4().hex, thing, 'deliver', input_=input_)

    def perform_action(self):
        print(self.input['selection'])
        time.sleep(1) # Start thread TODO
        self.thing.add_event(DeliveredEvent(self.thing, None))

class SelectionThing(Thing):
    """A Thing which updates its measurement every few seconds."""

    def __init__(self):
        Thing.__init__(
            self,
            'urn:dev:ops:my-selection-1234',
            'My Selection',
            [],
        )
        self.selection = Value([ 0, 0, 0, 0])
        self.add_property(
            Property(self,
                     'selection',
                     self.selection,
                     metadata=
                     {'title': 'selection',
                      'type': 'array',
                      }))
        self.add_available_action('deliver',
                                  { 'input' : {'type': 'object',
                                               'properties': { 'selection': {
                                               }}}}
                                  , DeliverAction)
        self.add_available_event('delivered', {})


def run_server():

    thing = SelectionThing()
    server = WebThingServer(SingleThing(thing), port=8888)
    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()

if __name__ == '__main__':
    run_server()
